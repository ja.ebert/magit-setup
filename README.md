# Magit Setup

A simple set-up for starting an Emacs session with
[Magit](https://magit.vc) enabled.

Magit is a high-level Git interface (“porcelain”) that enables an
accessible, exploratory, and completely interactive workflow.

To start Magit in your current directory, press `CTRL-x g` (first
press `CTRL` and `x` simultaneously, then press `g`).

## Installation

1. Install [Emacs](https://www.gnu.org/software/emacs/).
2. Add the `bin` directory in this repository to your `PATH`, for
   example in your `~/.bashrc`:

   ```shell
   export PATH="</absolute/path/to/this/repo>/bin:$PATH"
   ```

## Starting a Magit Session

```shell
magit
```

Then press `CTRL-x g` to execute `magit-status`. The `g` stands for
Git.

### Getting Help

In a Magit window (“buffer”), press `h` (for “help”) or `?`. You can
enter Magit by pressing `CTRL-x g`.

### Per-File Commands

To execute commands related to a file you are looking at, press
`CTRL-c`, then `ALT-g` or `META-g` to execute `magit-file-dispatch`.

### Arbitrary Commands

To execute arbitrary commands, you can also use `ALT-x` or `META-x`.
For example:

1. Press `ALT-x` or `META-x`.
2. Enter an arbitrary command, for example `magit-status` (what
   `CTRL-x g` also executes).

### Cancelling/Going Back

Press `CTRL-g` to cancel simple operations or go back up a level in
the Magit mappings.

### Confirming Advanced Operations

Press `CTRL-c CTRL-c` (“confirm”).

For example used to confirm Git commit messages or confirm interactive
rebases.

### Cancelling Advanced Operations

Press `CTRL-c CTRL-k` (“kancel”).

For example used to confirm Git commit messages or confirm interactive
rebases.

### Starting only Magit

You can also start an Emacs session with only Magit enabled, by
executing:

```shell
magit-only
```
